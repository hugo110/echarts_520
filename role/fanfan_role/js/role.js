//初始 echarts 
var fanfan_role = echarts.init(document.getElementById("fanfan_role"));

var sin = Math.sin;
var cos = Math.cos;
var pow = Math.pow;
var sqrt = Math.sqrt;
var cosh = Math.cosh;
var sinh = Math.sinh;
var exp = Math.exp;
var PI = Math.PI;
var square = function (x) {
    return x*x;
}
var mod2 = function (a, b) {
    var c = a % b;
    return c > 0 ? c : (c + b);
}

var theta1 = -(20/9) * PI;
var theta2 = 15 * PI;
function getParametricEquation(step) {
    return {
        u: {
            min: 0,
            max: 1,
            step: 1 / 24
        },
        v: {
            min: theta1,
            max: theta2,
            step: (theta2 - theta1) / step
        },
        x: function (x1, theta) {
            var phi = (PI/2)*exp(-theta/(8*PI));
            var y1 = 1.9565284531299512*square(x1)*square(1.2768869870150188*x1-1)*sin(phi);
            var X = 1-square(1.25*square(1-mod2((3.6*theta),(2*PI))/PI)-0.25)/2;
            var r = X*(x1*sin(phi)+y1*cos(phi));
            return r * sin(theta);
        },
        y: function (x1, theta) {
            var phi = (PI/2)*exp(-theta/(8*PI));
            var y1 = 1.9565284531299512*square(x1)*square(1.2768869870150188*x1-1)*sin(phi);
            var X = 1-square(1.25*square(1-mod2((3.6*theta),(2*PI))/PI)-0.25)/2;
            var r = X*(x1*sin(phi)+y1*cos(phi));
            return r * cos(theta);
        },
        z: function (x1, theta) {
            var phi = (PI/2)*exp(-theta/(8*PI));
            var y1 = 1.9565284531299512*square(x1)*square(1.2768869870150188*x1-1)*sin(phi);
            var X = 1-square(1.25*square(1-mod2((3.6*theta),(2*PI))/PI)-0.25)/2;
            var r = X*(x1*sin(phi)+y1*cos(phi));
            return X*(x1*cos(phi)-y1*sin(phi));
        }
    };
}
function  getOption(step,color){ //option = 
	return {
    toolbox: {
        feature: {
            saveAsImage: {
                backgroundColor: '#111'
            }
        },
        iconStyle: {
            normal: {
                borderColor: '#fff'
            }
        },
        left: 0
    },
    xAxis3D: {
        type: 'value'
    },
    yAxis3D: {
        type: 'value'
    },
    zAxis3D: {
        type: 'value'
    },
    grid3D: {

//      show: false,  //是否显示3d坐标系  默认 显示  true  为啥显示报错呢
        axisPointer: {
            show: false
        },
        axisLine: {
            lineStyle: {
                color: '#99e6ff' //坐标轴的颜色
            }
        },
        postEffect: {
            enable: true,
            SSAO: {
                enable: true,
                radius: 10,
                intensity: 2
            },
            edge: {
                enable: true
            }
        },
        temporalSuperSampling: {
            enable: true
        },
        light: {
            main: {
                intensity: 3,
                shadow: true,
            },
            ambient: {
                intensity: 0
            },
            ambientCubemap: {
                texture: 'data-1491896094618-H1DmP-5px.hdr',
                exposure: 0,
                diffuseIntensity: 1,
                specularIntensity: 0.5
            }
        },
        viewControl: {
            // projection: 'orthographic'
             autoRotate :true, //是否开启自动3d 旋转
             maxDistance :200, //最大的值
             minDistance  :200 //是距离 最小值   与最大值相等时  则不能够放大与缩小
        }
    },
    series: [{
        type: 'surface',
        parametric: true,
        shading: 'realistic',
        silent: true,
        wireframe: {
            show: false
        },
        realisticMaterial: {
            baseTexture: 'data-1494250104909-SkZtfeAyZ.jpg',
            roughness: 0.7,
            metalness: 0,
            textureTiling: [200, 20]
        },
        itemStyle: {
            color: color//'red'
        },
        parametricEquation: getParametricEquation(step)
    }]
};
}
//加载玫瑰  因为不知道是不是bug的原因 修改对象不可以了 只好完整替换对象
//if (option && typeof option === "object") {
    fanfan_role.setOption(getOption(1,'#fff')); //不进行多个玫瑰的合并
//}
//定时 动态改变数据
var stepValues = ["1","5","10","11","12","30","40","50","60","70","80","100","150","200","300","400","500","576"];
//var stepValues = ["500"];
//红玫瑰
var colors = ["#ffe5e5"," #ffcccc","#ffb3b3","#ff9999","#ff8080"," #ff6666","#ff4d4d","#ff3333","#ff1a1a","#ff0000"];//9个
//蓝玫瑰
//var colors = ["#e5f2ff"," #cce5ff","#b3d9ff","#99ccff","#80bfff","#66b2ff","#4da6ff","#3399ff","#1a8cff","#1a8cff"];//9个
var stopNum=0; //如果这个数大于数的长度 那么就停止计时器  18个

	var stopFlag=self.setInterval("role_change()",1000);
	function role_change(){
		//从新设置 玫瑰的成长度 
//		console.log(stepValues[stopNum]+"="+stepValues.length +"="+stopNum);
//		option.series.parametricEquation = getParametricEquation(getOption(stepValues[stopNum])); //数组中的一个值
//		if (option && typeof option === "object") {
	//改变颜色
			var color = colors[stepValues/2];
    		fanfan_role.setOption(getOption(stepValues[stopNum],color), true); //不进行多个玫瑰的合并
//		}
		//设置完成+1stopNum
		stopNum++;
		if(stopNum>=stepValues.length){ //如果这个数大于数的长度 那么就停止计时器
			stopFlag=window.clearInterval(stopFlag);
			var option = getOption(576,color);
			option.grid3D.viewControl.maxDistance=40;
			option.grid3D.viewControl.maxDistance=100;
			//证明成长完了玫瑰  同时  对玫瑰放开可以拖动大小，关闭坐标系
			fanfan_role.setOption(option, true); //不进行多个玫瑰的合并
			
		}
	}	
